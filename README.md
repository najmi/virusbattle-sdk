# VirusBattle SDK Usage Instructions


The VirusBattle SDK (vbSDK) is a set of scripts to access UL
Lafayette's VirusBattle Automated Malware Analysis webservice. 


Please visit [VirusBattle SDK Wiki](https://bitbucket.org/srl/virusbattle-sdk/wiki/Home) for more details.
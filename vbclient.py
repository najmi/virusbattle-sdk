#! /usr/bin/env python

import json
import os
import sys
import traceback
import logging
import requests
import re
import pprint
import progress

pp = pprint.PrettyPrinter(indent=4)


logger = logging

import virusbattle

def vbSuccess (response):
	assert('status' in response)
	return response['status'] == 'success'
def vbFailure (response):
	assert('status' in response)
	return 	response['status'] == 'failed'
def vbError (response):
	assert('status' in response)
	return response['status'] == 'error'


class SubmitJob (object):
	def __init__ (self, options):
		self.options = options
		if  not options.force:
			logger.debug("The switch -f (--force) was not used, a file will be processed only if its not already in the database")
		# open file to save the filehashes uploaded
		self.listfp = open(options.listfile, "a")

	def uploadFile (self, filepath):
		filename=os.path.basename(filepath)
		url =  virusbattle.make_vburl("upload")
		logger.info("[%s] Uploading file %s" % (filename,filepath))
		try:
			myFile = {'filedata': open(filepath, 'rb')}
			params={}
			# whether or not to force re-analysis, if already in the system
			# Use force carefully, and only if really necessary
			if self.options.force: params['force']='1'
			params['origFilepath'] = filepath
			# Password for decrypting zip file
			if self.options.password is not None:
				params['password'] = self.options.password
			try:
				params['unpackerConfig']=os.environ['VIRUSBATTLE_UNPACKER_CONFIG']#for customized configurations
			except:
				pass # use default config
			results = requests.post(url, files=myFile, params=params)
			logger.debug("[%s] Server returned: %s"%(filename,results.content))
			result=json.loads(results.text)	
			status = result.pop('status')
			filehash = result.pop('hash')
			if(status=="success"):
				logger.info("[%s] [%s] %s\n" % (filename,status, repr(result)))
				self.listfp.write(str(filehash)+"\n")
				return True
			else:
				logger.info("[%s] [%s] %s\n" % (filename, status, repr(result)))
		except Exception as e:
			logger.error("[Client-Error] [%s] unable to upload %s \n" % (sys.exc_info(),os.path.basename(filepath)))
			logger.error(repr(e)+"\n")
		return False

	def reprocess (self, objID):
		params = {}
		url =  virusbattle.make_vburl("reprocess", objID, **params)
		logger.debug("[%s] reprocessing using URL %s " % (objID, url))
		results = requests.get(url)
		return results
	def __enter__(self):
		return self
	def __exit__(self,type,value,tracebackObj):
		if value:
			#log error
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))



class Retrieve (object):
	def __init__ (self, options):
		self.options  = options
		self.threshold = options.threshold
	def file (self, objID):
		url = virusbattle.make_vburl("download", objID)
		logger.debug("[%s] Downloading using URL %s " % (objID, url))
		params={}
		if self.options.zipBinaryFiles:
			params['zipBinary']=1
		results = requests.get(url,params=params)
		return results
	def info (self, objID):
		url = virusbattle.make_vburl("query", objID)
		return self.getData(url, objID)

	def myuploads (self):
		url = virusbattle.make_vburl("query")
		results = requests.get(url)
		return results
	def matches (self, objID):
		params = {}
		if self.options.upperhalf:
			params['upperhalf'] = 1
		if self.threshold:
			params['threshold'] = self.threshold
		url = virusbattle.make_vburl("search/binary", objID)
		results = requests.get(url, params=params)
		return results

	def juice (self, objID):
		#objID here is the BinaryID or ProcedureID
		#ProcedureID is of the form BinaryID/0x<rva>
		url=''
		params=dict()
		if self.options.nolibproc:
			params['noLibProc']=True
		if objID.find('/') == -1:
			#binaryID
			url = virusbattle.make_vburl("show/binary", objID)
		else:
			#procID
			url = virusbattle.make_vburl("show/proc",objID)
		return self.getData(url, objID,params)

	def simInfo (self, objID):
		#objID here is the BinaryID or ProcedureID
		#ProcedureID is of the form BinaryID/0x<rva>
		url=''
		params=dict()
		if self.options.nolibproc:
			params['noLibProc']=True
		if objID.find('/') == -1:
			#binaryID
			url = virusbattle.make_vburl("search/procs",objID)
		else:
			#procID
			url = virusbattle.make_vburl("search/procs",objID)
		return self.getData(url, objID,params) 

	def getData (self, url, objID,params=None):
		try:
			logger.debug("[%s] Querying info using URL %s " % (objID, url))
			results = requests.get(url,params=params)
			logger.debug("[%s] Server returned: %s"%(objID,results.content))
			results = json.loads(results.text)
			return virusbattle.check_version(results)
		except virusbattle.VBVersionError as e:
			print repr(e)
			sys.exit (1)
		except Exception as e:
			logger.error("[Client-Error] [%s] unable to retrieve %s \n" % (sys.exc_info(),objID))
			logger.error(repr(e)+"\n")	
			return ''
			
	def __enter__(self):
		return self
	def __exit__(self,type,value,tracebackObj):
		if value:
			#log error
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))

			
		

#------------------------------------------------------------------------------
objectClassFileType = {
	'binary.unpacked.zip': 'unp.zip',
	'binary.unpacked': 'unp.exe',
	'binary.pe32': 'exe',
	"archive.zip": "zip",
	"archive.tar": "tar",
	"archive.tgz": "tgz",
	"archive.7z" : "7z",
	"json.juice" : "juice.json",
	"json.strings": "strings.json",
	"json.apiflowgraph":"apiflowgraph.json",
	"dot.callgraph":"callgraph.dot"
}

def is_original_class (object_class):
	return 'binary.pe' in object_class or 'archive' in object_class

def is_malware_class (object_class):
	return 'binary' in object_class or 'archive' in object_class or 'unhandled' in object_class

class VBVisitor (object):
	def visit (self, objID, result):
		pass
	def __enter__(self):
		return self
	def __exit__(self,type,value,tracebackObj):
		if value:
			#log error
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))

class QueryVisitor (VBVisitor):
	def __init__ (self, options):
		self.options = options
	def __enter__(self):
		print '['
		self.sep=""
		return self
	def __exit__(self,type,value,tracebackObj):
		print ']'
		if value:
			#log error
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))
		
 	def visit (self, objID, result):
		print self.sep+json.dumps(result, indent=2)
		self.sep=",\n"


class MatchVisitor (VBVisitor):
	def __init__ (self, options):
		self.options = options
		self.csv_filename = os.path.join(options.outdir, "similarity" + ".csv")
		self.json_filename = os.path.join(options.outdir, "similarity" + ".json")
		self.visited = {}
	def open (self):
		self.csv_fp = open(self.csv_filename,"w")
		self.json_fp = open(self.json_filename,"w")
		self.csv_fp.write("binary1,binary2,similarity\n")
		self.json_fp.write("[\n")
		self.counter = 0
		self.json_sep = "# %d\n" % self.counter
	def write(self, match):
		queryID = match['queryID']
		for m in match['matches']:
			try:
				self.csv_fp.write("%s,%s,%s\n" %(queryID, m['fileHash'], m['similarity']))
			except:
				self.csv_fp.write("%s,%s,%s\n" %(queryID, m['_id'], m['similarity']))
		self.json_fp.write(self.json_sep)
		self.json_fp.write(pp.pformat(match))
		self.counter +=1
		self.json_sep = ",\n# %d\n" % self.counter

	def close (self):
		self.csv_fp.close()
		self.json_fp.write("\n]")
		self.json_fp.close()
	def getMatches (self,objID):
		results = Retrieve(self.options).matches(objID)
		results = json.loads(results.text)
		if results['status'] == "success":
			results = results['answer']
			results['queryID'] = objID
		else:
			# create a dummy result, so rest of the processing takes place
			results = {'queryID':objID, 'matches':[], 'message':"No match found"}
		return results

	def visit (self, objID, result):
		# get similarity, but do it only once
		if self.visited.get(objID, 0) == 0:
			matches = self.getMatches(objID)
			matches['object_class'] = result['object_class']
			if result['object_class'] == "binary.unpacked":
				matches['parents'] = result.get('parents',[])
			self.write(matches)
		self.visited[objID] = 1

	def __enter__ (self):
		self.open()
		return self
	def __exit__(self, type, value, tracebackObj):
		self.close()
		if value:
			raise



def ensure_dir(path):
	try: 
		os.makedirs(path)
	except OSError:
		if not os.path.isdir(path):
			raise

class ProgressMonitorVisitor (VBVisitor):
	"""
	 Produces CSV files containing progress indications
	 Format: PE-Sha1,Unpacker_Status,Juice_Status
	"""
	def __init__ (self, options):
		self.options = options
		self.service_list = virusbattle.service_list()
		self.service_header = [re.match("srl(.*)", x).group(1)+"Status" for x in self.service_list]
	def __enter__(self):
		# create output directory, if it doesn't exists
		self.outdir = options.outdir
		ensure_dir (self.outdir)
		self.open_progress ()

	def open_progress (self):
		filename = os.path.join(self.outdir, 'vb-'+"progress"+'.csv')
		#logger.warn("Writing status check to %s" % filename)
		#self.outfp = open(filename, 'w')
		self.write_progress("PE-sha1,"+ ",".join(self.service_header))
		
	def write_progress (self, message):
		print message
		#self.outfp.write("%s\n"% message)
	def close_progress (self):
		#self.outfp.close ()
		pass

	def __exit__(self,type,value,tracebackObj):
		if value:
			#log error
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))
		self.close_progress()

	def default_status (self, object_class):
		progress = {}
		for x in self.service_list: progress[x] = "Pending"
		if object_class == "binary.unpacked":
			progress['srlUnpacker'] = 'n/a'
		return progress
	def visit (self, objID, result):
		logger.debug('[%s] Visiting with result %r' % (objID, result))
		object_class = result['object_class']
		if object_class in ['binary.pe32', 'binary.unpacked']:
			progress = self.default_status(object_class)
			children = result.get('children', [])
			for child in children:
				service_name = str(child['service_name'])
				progress[service_name]=child['status']
			logger.debug("%s,%r"%(objID,repr(progress)))
			self.write_progress("%s,%s,%s" %(objID, object_class,",".join([progress[x] for x in self.service_list])))

class MapVisitor (VBVisitor):
	"""
	 Produces CSV map files containing parent-child relationships
	"""
	def __init__ (self, options):
		self.options = options
	def __enter__(self):
		# create output directory, if it doesn't exists
		self.outdir = options.outdir
		ensure_dir (self.outdir)
		
		
	def __exit__(self,type,value,tracebackObj):
		if value:
			#log error
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))

	def get_csv_file(self, service_name):
		return os.path.join(self.outdir, 'vb-'+service_name+'.map')

	def visit (self, objID, result):
		logger.debug('[%s] Visiting with result %r' % (objID, result))
		children = result.get('children', [])
		for child in children:
			service_name = str(child['service_name'])
			childID = child['child']
			# skip, if childID is none
			if childID is None: continue
			# append the pair (objID and childID) in appropriate cs csvfile
			filename = self.get_csv_file (service_name)
			with open(filename, 'a') as out:
				logger.debug("[%s] Child: %s, type: %s" %(objID, childID, service_name) )
				out.write("%s,%s\n" %(objID, childID))

class ReprocessVisitor (VBVisitor):
	def __init__ (self, options):
		self.options = options
	def visit (self, objID, result):
		SubmitJob(self.options).reprocess(objID)

class DownloadVisitor (VBVisitor):
	def __init__ (self, options):
		self.retriever = Retrieve (options)
		self.options = options
		self.outdir = options.outdir
		try:
			os.makedirs(options.outdir)
		except:
			if not os.path.isdir(options.outdir):
				logger.error("Error: %s is not a directory %s" % (options.outdir, traceback.format_exc()))
				sys.exit(1)
	def visit (self, objID, result):
		object_class = result.get("object_class", None)
		if self.options.disable_malware_download and is_malware_class (str(object_class)):
			# do not download files that may contain malware, unless explicitly asked
			logger.debug("[%s] --enable_malware_download is not set. Ignoring download, object of class %s may contain malware." % (objID, str(object_class)))
			return

		if (not self.options.downloadall) and is_original_class (str(object_class)) :
			# do not download original files, unless explicitly asked
			logger.debug("[%s] --downloadall is not set. Ignoring download. It's of class %s. Binary" % (objID, str(object_class)))
			return

		filename = os.path.join(self.outdir, objID)
		filext = None
		if not object_class is None:
			# get file ext for the class. If there is none use the class itself as an extension
			filext = objectClassFileType.get(object_class, None)
			if (self.options.zipBinaryFiles) and ('binary' in object_class):
				filext='zip'
		if filext is not None:
			filename = filename + "." + filext

		logger.info ("[%s] Downloading to %s" % (objID, filename))
		with open(filename, "w") as ofp:
			ifp = self.retriever.file(objID)
			for chunk in ifp.__iter__():
				ofp.write (chunk)

class SimQueryProcessor (object):
	def __init__ (self, visitor, options):
		self.options = options
		self.visitor = visitor
		self.retriever = Retrieve(options)

	def filter(self, answer):
		if options.limit=='H': #keep semantically equivalent procedures only
			for rva in answer.keys():
				answer[rva]=answer[rva].pop('semantically_equivalent_procedures',[])
				if answer[rva] == list():
					answer.pop(rva)
		elif options.limit=='L': #keep semantically similar procedures only
			for rva in answer.keys():
				answer[rva]=answer[rva].pop('similar_procedures',[])
				if answer[rva] ==list():
					answer.pop(rva)				
		pass

	def main(self, objID):
		if objID is None:
			return
		if self.options.action == "show":
			# show juice results for a binary or procedure
			response =self.retriever.juice(objID)
		elif self.options.action == "search":
			# if objID is for proc, go about the business
			if not objID.find('/') == -1:
				response = self.retriever.simInfo(objID)
				answer = response['answer']
				# clean up the answer
				remove_key = []
				for rec in answer:
					# for i,x in enumerate(answer[rec]):
					# 	try:
					# 		binID =  x.pop('binary_id')
					# 		procRVA = x.pop('proc_rva')
					# 		matchID = binID + "/" + procRVA
					# 		x['proc_id'] = matchID
					# 	except:
					# 		print x
					# 		raise
					# answer[rec] = [x for x in answer[rec] if not x['proc_id'] == objID]
					if len(answer[rec]) == 0:
						remove_key.append(rec)
				for key in remove_key: answer.pop(key)
				if len(answer.keys()) ==0:
					# for this procedure, it has no match at all
					return
				answer['proc_id'] = objID
				response['answer'] = answer
			else:
				# get the juice, get procs from juice
				response = self.retriever.juice(objID)
				rva_list = response['answer'].keys()

				for i,rva in enumerate(rva_list):
					# ignore uninteresting procedures
					# ignore procedures with null gen_semantics
					genSemantics = response['answer'][rva]['gen_semantics']
					count_genSemantics = sum(map(len, genSemantics))
					if count_genSemantics < 10: continue

					procID = objID + "/" + rva
					self.main(procID)
				return
		try:
			if not vbSuccess(response):
				logging.error("[%s] Request failed: %s" % (objID, response['message']))
			else:
				logging.debug("[%s] Response is %s" % (objID, repr(response)))
				answer = response['answer']
				if self.options.limit is not None and self.options.action =='search':
					#filter out responses...
					self.filter(answer)
				#filtered
				self.visitor.visit (objID, answer)
		except virusbattle.VBVersionError as e:
			print repr(e)
			pass
		except Exception as e:
			logging.error("[%s] Query failed: server response : %s" % (objID, str(response)))
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))

class QueryProcessor (object):
	def __init__ (self, visitor, options):
		self.options = options
		self.visitor = visitor
		self.retriever = Retrieve(options)

	def filterServices(self,answer):
		# visit all the children, and keep only the services needed
		answer['children']=[c for c in answer.get('children',[]) if not c['service_name'] in virusbattle.serviceFilterList]

	def main(self, objID):
		if objID is None or objID == '':
			return
		logging.debug("[%r] QueryProcessor.main" % objID)
		response = self.retriever.info(objID)
		try:
			if not vbSuccess(response):
				logging.error("[%s] Query failed: %s" % (objID, response['message']))
			else:
				logging.debug("[%s] Response is %s" % (objID, repr(response)))
				answer = response['answer']
				#filter out experimental services' responses...
				self.filterServices(answer)
				self.visitor.visit (objID, answer)
				if self.options.recursive:
					children = answer.get('children',[])
					for c in children:
						if (c.get('status',"failure")=='success'):
							childID = c.get("child", "")
							logging.debug("[%s] Visiting child %r" % (objID,childID))
							self.main(childID)
						else:
							logging.info("[%s] service %s reported failure. Service_Data: %s"%(objID,c['service_name'],repr(c['service_data'])))
		except virusbattle.VBVersionError as e:
			print repr(e)
			pass
		except Exception as e:
			logging.error("[%s] Query failed: server response : %s" % (objID, str(response)))
			exc_type, value, exc_traceback =sys.exc_info()
			tb= repr(traceback.extract_tb(exc_traceback))
			logger.error('Error type: %s\nError Value: %s\nTraceback:%s'%(str(exc_type),str(repr(value)),str(tb)))

class SubmitProcessor (object):
	def __init__ (self, visitor, options):
		self.visitor = visitor
		self.recursive = options.recursive
		self.test = options.test

	def main (self, path):
		path = os.path.abspath(path)
		if os.path.isfile(path):
			self.submitFile(path)
		elif os.path.isdir (path):
			if self.recursive:
				self.submitDirectoryRecursive(path)
			else:
				self.submitDirectoryOneLevel(path)
	def submitFile (self, filename):
		logging.debug("Visiting file: %s", filename)
		if not options.test:
			self.visitor.uploadFile (filename)

	def submitDirectoryOneLevel (self, path):
		logging.debug("Traversing directory -- ONE LEVEL: %s", path)
		files = os.listdir(path)
		# make full path
		files = map(lambda x: os.path.join(path, x), files)
		# Keep only files (not directory)
		files = filter(os.path.isfile, files)
		for filepath in files:
			self.submitFile (filepath)

	def submitDirectoryRecursive (self, path):
		logging.debug("Traversing directory Recursively: %s", path)
		for root, dirs, files in os.walk(path):
			relative_path = root # os.path.relpath(root, path)
			for filename in files:
				filepath = os.path.join(relative_path, filename)
				self.submitFile(filepath)


def readHashes (filename):
	if os.path.exists(filename):
		with open(filename, 'r') as fp:
			return map(lambda x: x.strip('\n\t\r'),set(fp.readlines()))
	return []


def getMyUploads (options):
    results = Retrieve(options).myuploads()
    results = json.loads(results.text)
    if results['status'] == "success":
        results = results['answer']
    else:
        results = []
    uploadfile = os.path.join(options.outdir, "uploads.txt")
    with open(uploadfile, "w") as fp:
	    for r in results:
		    fp.write("%s\n"% r['_id'])



action_list="upload,reprocess,query,download,map,status,show,search,matches,myuploads".split(",")
default_action="upload"
default_outdir="./Results"
validArgs    = "force,password,verbose".split(",")
default_listfile = "UploadedHashes.txt"

loglevels = {"info": logging.INFO, "debug": logging.DEBUG, "warn": logging.WARN, "error": logging.ERROR}

default_loglevel = "warn"

if __name__ == "__main__":	
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option ("-f", "--force", dest="force", default=False, action="store_true",
		       help="Force resubmission, if the file already exists")
    parser.add_option ("-p", "--password", dest="password", default=None,
		       help="Password for Zip and 7z encrypted archives")
    parser.add_option ("-a", "--action", dest="action", default=default_action,
		       help="Action to perform. One of %s. Default is: %s" % (", ".join(action_list), default_action))
    parser.add_option ("-l", "--limit", dest="limit", default=None,
		       help="limit similarity search results to semantically equilvalent (High) or similar procedures (Low) only.")
    parser.add_option ("-o", "--outdir", dest="outdir", default=default_outdir,
		       help="Directory to save downloaded files. Default is: %s" % (default_outdir))
    parser.add_option("--norecursive",
                      action="store_false", dest="recursive", default=True,
                      help="Do not recursively visit children.")
    parser.add_option("--fullmatrix",
                      action="store_false", dest="upperhalf", default=True,
                      help="Get full matrix search; default upperhalf only")
    parser.add_option("--threshold",
                      dest="threshold", default=None,
                      help="Threshold for similarity matching")
    parser.add_option("--xl", "--noLibrary",
                      action="store_true", dest="nolibproc", default=False,
                      help="eXclude Library functions from juice and similarity responses")
    parser.add_option("-v", "--verbose",
                      action="store_true", dest="verbose", default=False,
                      help="Verbose output")
    parser.add_option("--test",
                      action="store_true", dest="test", default=False,
                      help="Do a test run, don't actually upload")
    parser.add_option("--lf", "--list-file", dest="listfile", default=default_listfile,
		      help="File to keep list of filehashes that are uploaded. Default is: %s" % default_listfile)
    parser.add_option("--loglevel", dest="loglevel", default=default_loglevel,
                      help="Select log level. One of: %s. Default is: %s" % (", ".join(loglevels.keys()), default_loglevel)) 
    parser.add_option("--downloadall", dest="downloadall", default=False, action="store_true",
                      help="Download all files. By default only unpacked files are downloaded")
    parser.add_option("--zipbinary", dest="zipBinaryFiles", default=False, action="store_true",
                      help="Download binary files as zip. Default as .exe file")
    parser.add_option("--enable_malware_download", dest="disable_malware_download",default=True, action="store_false",
                      help="Download Malware files. Malware download is disabled by default.")


    (options, args) = parser.parse_args()

    # setup log output level
    if options.verbose:
	    options.loglevel = "debug"
    if not options.loglevel in loglevels.keys():
	    print "ERROR - in correct log level %s, use -h to get help" % options.loglevel
	    sys.exit (1)
    logging.basicConfig(format='%(asctime)s %(message)s', level=loglevels[options.loglevel])


    # ensure there is something asked to do
    if not options.action in action_list:
	    print "ERROR - Unknown action %s,  use -h to get help" % options.action
	    sys.exit (1)
    if len(args) == 0 and not options.action == "upload":
	    args = readHashes (options.listfile)
    if len(args) == 0 and options.action not in ["myuploads"] :
	    print "Need arguments"
	    parser.print_help()
	    sys.exit(1)

    # by default, do not show progress 
    # it may be set to 1 for expected long running tasks
    # .... be careful though, those tasks should not output to the stdout
    # .... since the progress monitor also writes to the stdout
    progress_verbosity = 0
    # perform work based on -a (action) requested
    if options.action == "upload":
	    # upload binary for processing
	    visitor = SubmitJob(options)
	    processor = SubmitProcessor(visitor, options)
    elif options.action == "query":
	    # query results for a binary
	    visitor = QueryVisitor (options)
	    processor = QueryProcessor (visitor, options)
    elif options.action == "download":
	    # download unpacked binary, binary, or juice
	    visitor = DownloadVisitor (options)
	    processor = QueryProcessor (visitor, options)
    elif options.action == "reprocess":
	    # perform analysis of a binary again (without uploading)
	    visitor = ReprocessVisitor (options)
	    # Do not recurse, for re-processing is automatically recursive
	    options.recursive = False
	    processor = QueryProcessor (visitor, options)
    elif options.action == "map":
	    # query mappings for a binary
	    visitor = MapVisitor (options)
	    processor = QueryProcessor (visitor, options)
    elif options.action == "status":
	    # check status for a binary
	    visitor = ProgressMonitorVisitor (options)
	    processor = QueryProcessor (visitor, options)
    elif options.action in ["show", "search"]:
	    # show juice results for a binary or procedure
	    visitor = QueryVisitor (options)
	    processor = SimQueryProcessor (visitor, options)
    elif options.action == "matches":
	    # search for related binaries for a binary or related procedures for a procedure
	    visitor = MatchVisitor (options)
	    processor = QueryProcessor (visitor, options)
	    progress_verbosity = 1
    elif options.action == "myuploads":
	    getMyUploads(options)
	    sys.exit(1)
    else:
	    print "Invalid action %s, nothing to do" % options.action
	    sys.exit (1)

    with visitor as myvisitor:
	    progress_monitor = progress.ProgressMonitor(verbosity=progress_verbosity)
	    progress_monitor.start_next_stage(options.action)
	    progress_monitor.track_task(len(args), options.action, increment=1)

            for arg in args:
	    	processor.main (arg)
		progress_monitor.bump_task()
	    progress_monitor.done_task()
	    progress_monitor.done("Completed")
